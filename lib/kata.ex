defmodule Kata do
  def find_outlier(integers) do
    expected_remainder = integers |> expected_remainder()
    drop_filter = fn i -> i |> abs() |> rem(2) != expected_remainder end
    List.first(integers |> Stream.drop_while(drop_filter) |> Enum.take(1))
  end

  defp expected_remainder(integers) do
    case integers |> odd_or_even() |> Map.fetch(true) do
      {:ok, 1} -> 0
      :error -> 0
      _ -> 1
    end
  end

  defp odd_or_even(integers) do
    integers
    |> Stream.take(3)
    |> Enum.map(fn i -> rem(i, 2) == 0 end)
    |> Enum.reduce(%{}, fn b, map -> Map.update(map, b, 1, &(&1 + 1)) end)
  end
end